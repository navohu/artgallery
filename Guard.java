package gallery;

/**
 * Created by nathalievonhuth on 22/02/2016.
 */
public class Guard {
    public double guard_x;
    public double guard_y;

    public Guard(double x, double y){
        this.guard_x = x;
        this.guard_y = y;
    }
    
    public double getX(){
    	return guard_x;
    }
    
    public double getY(){
    	return guard_y;
    }
    
    public void setX(double x){
    	guard_x = x;
    }
    
    public void setY(double y){
    	guard_y = y;
    }
    
}
