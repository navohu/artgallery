package gallery;

/**
 * Created by nathalievonhuth on 22/02/2016.
 */
public class Vertice {
    public double vertices_x;
    public double vertices_y;

    public Vertice(double x, double y){
        this.vertices_x = x;
        this.vertices_y = y;
    }
    
    public double getX(){
    	return vertices_x;
    }
    
    public double getY(){
    	return vertices_y;
    }
}
