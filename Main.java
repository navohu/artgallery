package artGallery;

import java.applet.Applet;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.*;

import javax.swing.JFrame;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



public class Main extends Applet {
	
    static ArrayList<Guard> guard = new ArrayList<Guard>();

    
    @SuppressWarnings("unchecked")
	public static void placeGuards(ArrayList<Vertice> vertices, BufferedWriter out, JSONArray array) throws IOException{
  
    	int count = 0;
    	int limit = vertices.size() / 3;
    	for(int i = 0; i < vertices.size(); i++)
    	{
    		if(count >= limit)
    		{
    			break;
    		}
    		
    		Guard a = new Guard(0,0);
    		if(i % 3 == 0)
    		{
    			a.guard_x = vertices.get(i).vertices_x;
    			a.guard_y = vertices.get(i).vertices_y;
    			
    				//create JSON objects to hold x and y variables
        			JSONObject coords = new JSONObject();
        			coords.put("x", a.guard_x);
        			coords.put("y", a.guard_y);
        			//add co-ordinates to array
        			array.add(coords);
    			
    			guard.add(a);
    			
    			//determines where comma should be placed
    			if(i + 3 >= vertices.size() -2)
    			{
    			out.write("("+ a.guard_x +", "+ a.guard_y+")");
    			}
    			else
    			{
    				out.write("("+ a.guard_x + ", "+a.guard_y+"), ");
    			}
    			count++;
    			
    		}
    		
    	}
    	
    	//System.out.println(count);
    	
    }
    	
    public static void removeRandomLines(Scanner scanner)
    {
    	int lineCount = 0;
		while(lineCount<8){
			scanner.nextLine();
			lineCount++;
		}
    }
    
    public static void removePolygonNumbers(Scanner scanner, ArrayList<String> stuff)
    {
    	while(scanner.hasNextLine()){
			String y = scanner.nextLine();
			int colon_index = y.indexOf(':');
			String new_y = y.substring(colon_index + 2,y.length()-1);
			//System.out.println(new_y);
			stuff.add(new_y);
		}
    }
    
    public static void separateCoordinates(ArrayList<String> stuff, HashMap<Integer, ArrayList<String>> map)
    {
    	for(int i = 0; i < stuff.size();i++){
			int front_bracket = 0;
			ArrayList<String> points = new ArrayList<String>();
			
			for(int j = 0; j < stuff.get(i).length(); j++){
				if(stuff.get(i).charAt(j) == '('){
					front_bracket = j;
				}
				else if(stuff.get(i).charAt(j) == ')'){
					points.add(stuff.get(i).substring(front_bracket, j+1));
				}
			}	
			map.put(i, points);						
		}	
    }
    
    public static int [] numberOfVertices(HashMap<Integer, ArrayList<String>> map)
    {
    	int [] numOfVertices = new int [map.size()];
		for(int k = 0; k < map.size(); k++){
			numOfVertices[k] = map.get(k).size();			
		}
		
		return numOfVertices;
    }
    
    public static void assignCoordinates(HashMap<Integer, ArrayList<String>> map, HashMap<Integer, ArrayList<Double>> mapFinal)
    {
    	for(int n = 0; n < map.size(); n++){			
			ArrayList<Double> coords = new ArrayList<Double>();
			
			for(int m=0;m<map.get(n).size();m++){
				int front_bracket = 0;
				int comma = 0;		
				
				for(int p=0;p<map.get(n).get(m).length();p++){
					if(map.get(n).get(m).charAt(p) == '('){
						front_bracket = p;						
					}
					else if(map.get(n).get(m).charAt(p) == ','){
						comma = p;
					}
					else if(map.get(n).get(m).charAt(p) == ')'){
						coords.add(Double.parseDouble(map.get(n).get(m).substring(front_bracket+1, comma)));
						coords.add(Double.parseDouble(map.get(n).get(m).substring(comma+2, p)));				
					}
				}					
			}			
			mapFinal.put(n, coords);		
		}
    }
    
    public static void removeSemiColon(ArrayList<String> stuff, ArrayList<String> vertices, ArrayList<String>guards)
    {
    	for(int i = 0; i < stuff.size();i++){
    		
			for(int j = 0; j < stuff.get(i).length(); j++){
				if(stuff.get(i).charAt(j) == ';'){
					
					vertices.add(stuff.get(i).substring(0, j));
					guards.add(stuff.get(i).substring(j+1));
					
				}
			}	
								
		}	
    }
    
	public static HashMap<Integer, ArrayList<Double>> parser(String inputFile) throws FileNotFoundException{
			
			Scanner scanner = new Scanner(new File(inputFile));	
			HashMap<Integer, ArrayList<String>> map = new HashMap<Integer, ArrayList<String>>();
			HashMap<Integer, ArrayList<Double>> mapFinal = new HashMap<Integer, ArrayList<Double>>();
			
			ArrayList<String> stuff = new ArrayList<String>();
			removeRandomLines(scanner);
			removePolygonNumbers(scanner, stuff);
			//remove semi colon
			
			separateCoordinates(stuff, map);
			
			int [] numOfVertices = numberOfVertices(map);
						
			assignCoordinates(map, mapFinal);
			scanner.close();
			return mapFinal;
	}
	
	public static ArrayList<ArrayList<Double[][]>> parsePartTwo(String inputFile) throws FileNotFoundException {
		
		Scanner scanner = new Scanner(new File(inputFile));

		ArrayList<String> stuff = new ArrayList<String>();
		ArrayList<String> vertices = new ArrayList<String>();
		ArrayList<String> guards = new ArrayList<String>();
		
		HashMap<Integer, ArrayList<String>> verticesMap = new HashMap<Integer, ArrayList<String>>();
		HashMap<Integer, ArrayList<String>> guardsMap = new HashMap<Integer, ArrayList<String>>();
		
		HashMap<Integer, ArrayList<Double>> vMapFinal = new HashMap<Integer, ArrayList<Double>>();
		HashMap<Integer, ArrayList<Double>> gMapFinal = new HashMap<Integer, ArrayList<Double>>();
		
		//remove the random lines and polygon number + semi colon
		removeRandomLines(scanner);
		removePolygonNumbers(scanner, stuff);
		
		//separate guards coordinates and vertices coordinates
		removeSemiColon(stuff, vertices, guards);
		
		//separate string to (x,y) format for each vertice and each guard
		separateCoordinates(vertices, verticesMap);
		separateCoordinates(guards, guardsMap);
		
		//separate (x,y) to x and y in double format for guards and vertices
		assignCoordinates(verticesMap, vMapFinal);
		assignCoordinates(guardsMap, gMapFinal);
		
		//arraylists containing coordinates in 2d arrays of each polygon's vertices and guards
		ArrayList<Double [][]> vertA = new ArrayList<Double [][]>();
		ArrayList<Double [][]> guarA = new ArrayList<Double [][]>();
		
		for(int i = 0; i < vMapFinal.size(); i++)	//for number of polygons
		{

			Double [][] coords = coors2(vMapFinal.get(i), vMapFinal.get(i).size());
			Double [][] coords2 = coors(gMapFinal.get(i), gMapFinal.get(i).size());
			vertA.add(coords);
			guarA.add(coords2);
			
		}
		
		//index 0 is arraylist of all polygons and their vertices
		//index 1 is arraylist of all guards and their coordinates
		ArrayList<ArrayList<Double [][]>> vertAndGuards = new ArrayList<ArrayList<Double [][]>>();
		vertAndGuards.add(vertA);
		vertAndGuards.add(guarA);
		return vertAndGuards;

	}
     
    
    public static void createVertices(Double[][]coors, int size, int element, BufferedWriter out, JSONArray array) throws IOException{
    	int sizeArray = size/2;
        ArrayList<Vertice> vertices = new ArrayList<Vertice>();
    
    	for(int point = 0; point < sizeArray; point++){
    		Double x = coors[0][point];
    		Double y = coors[1][point];
    		
    		vertices.add(point,new Vertice(x,y));
    	}
    	
    	placeGuards(vertices, out, array);
	}
        	  
    public static Double[][] coors(ArrayList<Double> list, int length){
    	Double [][] coordinates = new Double [2][length/2];
    	int count = 0;
    	for(int j = 0; j < length; j++){
    		if((j+1)%2 != 0){
    			coordinates[0][count] = list.get(j);
    		}
    		else{
    			coordinates[1][count] = list.get(j);
    			count++;
    		}
    	}
    	return coordinates;
    }
    
    public static Double[][] coors2(ArrayList<Double> list, int length){
    	Double [][] coordinates = new Double [2][length];
    	int count = 0;
    	
    	for(int j = 0; j < length; j++){
    		int k = j+1;
    		if((j+1)%2 != 0){
    			coordinates[0][count] = list.get(j);
    		}
    		else{
    			coordinates[1][count] = list.get(j);
    			count++;
    			//add midpoints
    			if(j == (length-1)){
    				coordinates[0][count] = (list.get(j-1)+list.get(0))/2;	//x midpoint
        			coordinates[1][count] = (list.get(j)+list.get(1))/2;	//y midpoint
        			count++;
    			}
    			else{
    				coordinates[0][count] = (list.get(j-1)+list.get(k))/2;	//x midpoint
        			coordinates[1][count] = (list.get(j)+list.get(k+1))/2;	//y midpoint
        			count++;
    			}    			
    		}  		
    		
    	}
    	return coordinates;
    }  
   
   
    @SuppressWarnings("unchecked")
	public static void createFiles(HashMap<Integer, ArrayList<Double>> map)
    {
    	try {
    		//create .txt file and add team name and password
    		BufferedWriter out = new BufferedWriter(new FileWriter("output.txt"));	
    		out.write("moose");
    		out.newLine();
    		out.write("ttaemp7g04u7no62bmklnafpes");
    		out.newLine();
    		
    		//create new .json file
    		File file=new File("guardsresult.json");  
            file.createNewFile();  
            FileWriter fileWriter = new FileWriter(file); 
            
            //change index for specific guard
            Double [][] dave = coors(map.get(0),map.get(0).size());
            JSONArray array = new JSONArray();
            createVertices(dave,map.get(0).size(),0, out, array);
         
            fileWriter.write(array.toJSONString());
        	fileWriter.write("\r\n");
            	
    		for(int p = 0; p < map.size(); p++){

        		array = new JSONArray();
        		
        		
            	dave = coors(map.get(p),map.get(p).size());
            	out.write((p+1)+": ");
            	createVertices(dave,map.get(p).size(),p, out, array);
            	out.newLine();
            
            	          	
        		}
    		
    		System.out.println("Output.txt and GuardsCoordinates.json have been created. Check workspace for files");
    		out.close();  
            fileWriter.flush();  
            fileWriter.close(); 
            
    		}
    	
    	catch (IOException e) {}
   
       }
    
    @SuppressWarnings("unchecked")
	public static void createPolygonJSON(HashMap<Integer, ArrayList<Double>> map) throws IOException
    {
    	File file=new File("segment.json");  
        file.createNewFile();  
        FileWriter fileWriter = new FileWriter(file); 
        
        //change this to change the polygon printed, remember to make it corresspond to json for guards
    	for(int i = 0; i < 1; i++)
    	{
    		JSONArray array = new JSONArray();
    		JSONObject vector;
    		JSONObject a;
    		JSONObject b;
    		
    		for(int j = 0; j < map.get(i).size()-2; j+=2)
    		{
    			int k = j;
    			vector = new JSONObject();
    			a = new JSONObject();
    			b = new JSONObject();
    			
    			a.put("x", map.get(i).get(k));
    			a.put("y", map.get(i).get(k+1));
    			
    			vector.put("a", a);
    			
    			b.put("x", map.get(i).get(k+2));
    			b.put("y", map.get(i).get(k+3));
    			vector.put("b", b);
    			
    			//add vectors a and b to the array
    			array.add(vector);	
    			
    		}
    	//add first and last
    		a = new JSONObject();
    		b = new JSONObject();
    		vector = new JSONObject();
    		
    		a.put("x", map.get(i).get(0));
			a.put("y", map.get(i).get(1));
			
			vector.put("a", a);
			
			b.put("x", map.get(i).get(map.get(i).size()-2));
			b.put("y", map.get(i).get(map.get(i).size()-1));
			
			vector.put("b",b);
			array.add(vector);
			
        	fileWriter.write(array.toJSONString());
        	fileWriter.write("\r\n");
    	}
    	System.out.println("Polygons.json has been created");
    	fileWriter.flush();  
        fileWriter.close();
    }

    public static void partTwo(ArrayList<ArrayList<Double[][]>> partTwoArrayList) 
    {
    	try {
    		BufferedWriter out = new BufferedWriter(new FileWriter("partTwo.txt"));	
    		out.write("moose");
    		out.newLine();
    		out.write("ttaemp7g04u7no62bmklnafpes");
    		out.newLine();
    	
    		for(int i = 0; i < partTwoArrayList.get(0).size(); i++) //for each polygon   partTwoArrayList.get(0).size()
        	{
    			out.write((i+1)+": "+lineTest.guardCheck(partTwoArrayList.get(0).get(i), partTwoArrayList.get(1).get(i), (i+1)));
        		out.newLine();
       
        	}
    		
    		System.out.println("partTwo.txt has been created");
    		out.close();
    	}
    	catch (IOException e) {}
    		
    }
    	
    public static void main(String[] args) throws IOException {    	
    	
    	//the file with 30 polygons
    	HashMap<Integer,ArrayList<Double>> map = parser("guards.txt");
    	
    	//create polygon json file
    	createPolygonJSON(map);
    	createFiles(map);
    	
//    	ArrayList<ArrayList<Double[][]>> partTwoArrayList = parsePartTwo("check.rtf");
//    	partTwo(partTwoArrayList);
    	
    	
    }
    
}
 


