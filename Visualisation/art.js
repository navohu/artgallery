var vis = d3.select("body").append("svg")
         .attr("width", 1000)
         .attr("height", 667),

scaleX = d3.scale.linear()
        .domain([-30,30])
        .range([0,600]),

scaleY = d3.scale.linear()
        .domain([0,50])
        .range([500,0]),

guards = [{"x":0.0, "y":25.0},
        {"x":8.5,"y":23.4},
        {"x":19.0,"y":15.5}],

poly = [{"x":0.0, "y":25.0},
        {"x":8.5,"y":23.4},
        {"x":13.0,"y":21.0},
        {"x":19.0,"y":15.5},
        {"x":10.0,"y":20.5},
        {"x":15.0,"y":15.5}];

vis.selectAll("polygon")
    .data([poly])
	.enter().append("polygon")
    .attr("points",function(d) { 
        return d.map(function(d) { return [scaleX(d.x),scaleY(d.y)].join(","); }).join(" ");})
    .attr("fill", "none")
    .attr("stroke","black")
    .attr("stroke-width",2);

var circles = vis.selectAll("circle")
				.data(guards)
				.enter()
				.append("circle");


var circleattributes = circles
						.attr("cx", function(d){ return [scaleX(d.x)]; })
						.attr("cy", function(d){ return [scaleY(d.y)]; })
						.attr("r", 3)
						.style("fill", "red");
var a,b,c
function getMinimum(array){
	array.sort();
	a = array[0];
	b = array[1];
	c = array[2];
}

/*Initializing x y variables*/
var guardX, guardY, polyX, polyY
var distanceGuardToPoint = []
function lineGuardToPoints(guards, poly){
	for (var i = 0; i < guards.length; i++) { //get the x and y of a single guard
		guardX = guards[i].x;
		guardY = guards[i].y;
		console.log("Guard x: " + guardX + " y: " + guardY);

		for (var j = 0; j < poly.length; j++) { //get the x and y for all other points
			polyX = poly[j].x;
			polyY = poly[j].y;
			distanceGuardToPoint[i] = Math.sqrt((polyX-guardX)^2 + (polyY-guardY)^2);
			getMinimum(distanceGuardToPoint);
			
			console.log("Poly x: " + polyX + " y: " + polyY);

			/*Draw a line between each guard and all the points*/
			var lineattributes = vis.append("line")
								.attr("x1", [scaleX(guardX)])
								.attr("x2", [scaleX(polyX)])
								.attr("y1", [scaleY(guardY)])
								.attr("y2", [scaleY(polyY)])
								.attr("stroke-width", 2)
								.attr("stroke", "blue");
			// console.log(lineattributes);
		}
	}
}

// lineGuardToPoints(guards,poly);

// var intersect = Intersection.intersectLinePolygon(guards[0], poly[3], poly);

// console.log(intersect.status);

	var svgIntersections = require('svg-intersections');
    var intersect = svgIntersections.intersect;
    var shape = svgIntersections.shape;
 
    var intersections = intersect(  
        shape("circle", { cx: 0, cy: 0, r: 50 }),
        shape("rect", { x: 0, y: 0, width: 60, height: 30 })  
    );




    var i;
		var ray = {
			for (i = 0; i < guards.length; i++) {
				a:{x:guards[i].x,y:guards[i].y},
				b:{x:guards[i].x+dx,y:guards[i].y+dy}
			}	
		};

		if ((guards[i].x*scale + centerX) < centerX && (guards[i].y * scale) < centerY) {
			guards[i].x = guards[i].x * scale + centerX + centerX/100; //push the guard a tiny bit toward the center
			guards[i].y = guards[i].y * scale + centerY/100;
		}
		else if ((guards[i].x*scale + centerX) < centerX && (guards[i].y * scale) > centerY) {
			guards[i].x = guards[i].x * scale + centerX + centerX/100; //push the guard a tiny bit toward the center
			guards[i].y = guards[i].y * scale - centerY/100;
		}
		else if ((guards[i].x*scale + centerX) > centerX && (guards[i].y * scale) < centerY) {
			guards[i].x = guards[i].x * scale + centerX - centerX/100; //push the guard a tiny bit toward the center
			guards[i].y = guards[i].y * scale + centerY/100;
		}
		else if ((guards[i].x*scale + centerX) > centerX && (guards[i].y * scale) > centerY) {
			guards[i].x = guards[i].x * scale + centerX - centerX/100; //push the guard a tiny bit toward the center
			guards[i].y = guards[i].y * scale - centerY/100;
		}
