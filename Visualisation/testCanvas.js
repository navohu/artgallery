//GUARDS
var guards = [
        {x:3, y:1},
        {x:5,y:0},
        {x:0.5,y:2}
];

var segments = [
	{a:{x:5,y:2}, b:{x:4.5,y:1}},
	{a:{x:4.5,y:1}, b:{x:4,y:1}},
	{a:{x:4,y:1}, b:{x:3.5,y:2}},
	{a:{x:3.5,y:2}, b:{x:3,y:1}},
	{a:{x:3,y:1}, b:{x:2.5,y:1}},
	{a:{x:2.5,y:1}, b:{x:2,y:2}},
	{a:{x:2,y:2}, b:{x:1.5,y:1}},
	{a:{x:1.5,y:1}, b:{x:1,y:1}},
	{a:{x:1,y:1}, b:{x:0.5,y:2}},
	{a:{x:0.5,y:2}, b:{x:0,y:0}},
	{a:{x:0,y:0}, b:{x:5,y:0}},
	{a:{x:5,y:0}, b:{x:5,y:2}}
];

/*Scales the polygon*/
function scaleSegment(){
	for(i in segments)
	{
			segments[i].a.x =  segments[i].a.x * scale + centerX;
			segments[i].a.y =  segments[i].a.y * scale + centerY;
			segments[i].b.x =  segments[i].b.x * scale + centerX;
			segments[i].b.y =  segments[i].b.y * scale + centerY;
	}
	for(i in guards)
	{
		guards[i].x = guards[i].x * scale + centerX; //push the guard a tiny bit toward the center
		guards[i].y = guards[i].y * scale + centerY;
	}
}
// Find intersection of RAY & SEGMENT
function getIntersection(ray,segment){
	// RAY in parametric: Point + Delta*T1
	var r_px = ray.a.x;
	var r_py = ray.a.y;
	var r_dx = ray.b.x-ray.a.x;
	var r_dy = ray.b.y-ray.a.y;
	// SEGMENT in parametric: Point + Delta*T2
	var s_px = segment.a.x;
	var s_py = segment.a.y;
	var s_dx = segment.b.x-segment.a.x;
	var s_dy = segment.b.y-segment.a.y;
	// Are they parallel? If so, no intersect
	var r_mag = Math.sqrt(r_dx*r_dx+r_dy*r_dy);
	var s_mag = Math.sqrt(s_dx*s_dx+s_dy*s_dy);
	if(r_dx/r_mag==s_dx/s_mag && r_dy/r_mag==s_dy/s_mag){
		// Unit vectors are the same.
		return null;
	}
	// SOLVE FOR T1 & T2
	// r_px+r_dx*T1 = s_px+s_dx*T2 && r_py+r_dy*T1 = s_py+s_dy*T2
	// ==> T1 = (s_px+s_dx*T2-r_px)/r_dx = (s_py+s_dy*T2-r_py)/r_dy
	// ==> s_px*r_dy + s_dx*T2*r_dy - r_px*r_dy = s_py*r_dx + s_dy*T2*r_dx - r_py*r_dx
	// ==> T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
	var T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx);
	var T1 = (s_px+s_dx*T2-r_px)/r_dx;
	// Must be within parametic whatevers for RAY/SEGMENT
	if(T1<0) return null;
	if(T2<0 || T2>1) return null;
	// Return the POINT OF INTERSECTION
	return {
		x: r_px+r_dx*T1,
		y: r_py+r_dy*T1,
		param: T1
	};
}
///////////////////////////////////////////////////////
// DRAWING
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
///////////////////////////////////////////////
var scale = 100;
var centerX= canvas.width/2;
var centerY= canvas.height/2;
var xcords = [];
var ycords = [];
/*Get all xcoords of segment into array*/
for (var i = 0; i < segments.length; i++) {
	xcords[i] = segments[i].a.x * scale + centerX;
}
/*Get all ycoords of segment into array*/
for (var i = 0; i < segments.length; i++) {
	ycords[i] = segments[i].a.y * scale + centerY;
}
/*Tests whether a point is inside or outside the polygon*/
function pnpoly( nvert, vertx, verty, testx, testy) {
	//returns true if the point is within the polygon
	//nvert = number of polygon nodes, vertx/y = arrays of all nodes, testx/y = guard
  var i, j, c = false;
  for( i = 0, j = nvert-1; i < nvert; j = i++ ) {
    if(((verty[i] > testy) != (verty[j] > testy)) &&
       (testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i])) {
           c = !c;
    }
  }
  return c;
}
/*Creates a 360 circle from a guards and finds a point that is within the polygon */
function getPointInPolyStart(guard, segments){
	var pointX, pointY;
	for (var angle = 0; angle < 360; angle++) {
		pointX = guard.x + 0.5* Math.sin(angle);
		pointY = guard.y - 0.5* Math.cos(angle);
		//console.log(guard.x, pointX);
		//console.log(guard.y, pointY);
		// console.log("Inside GetPointInPoly");
		for (var i = 0; i < segments.length; i++) {
			// console.log("Inside For Loop");
			if (pnpoly(segments.length, xcords, ycords, pointX, pointY)) {
				console.log(pointX);
				console.log(pointY);
				return {x:pointX, y:pointY};
			}
		}
	}
}

function getPointInPolyEnd(guard, segments){
	var pointX, pointY;
	for (var angle = 360; angle > 0; angle--) {
		pointX = guard.x + 0.5* Math.sin(angle);
		pointY = guard.y - 0.5* Math.cos(angle);
		//console.log(guard.x, pointX);
		//console.log(guard.y, pointY);
		// console.log("Inside GetPointInPoly");
		for (var i = 0; i < segments.length; i++) {
			// console.log("Inside For Loop");
			if (pnpoly(segments.length, xcords, ycords, pointX, pointY)) {
				console.log(pointX);
				console.log(pointY);
				return {x:pointX, y:pointY};
			}
		}
	}
}
/*Creates an array of the new guards that are inside the polygon and not on the line*/
var visibilityGuard = [];
function getAllVisibility(){
	var j = 0;
	for (var i = 0; i < guards.length; i++, j+=2) {
		visibilityGuard[j] = getPointInPolyStart(guards[i], segments);
		visibilityGuard[j+1] = getPointInPolyEnd(guards[i], segments);
	}
	// console.log(visibilityGuard);
	return visibilityGuard;
}

/////////////////////////////// DRAW
function draw(){
	getAllVisibility();
	// Clear canvas
	ctx.clearRect(0,0,canvas.width,canvas.height);
	ctx.canvas.width  = window.innerWidth;
  	ctx.canvas.height = window.innerHeight;
	// Draw segments
	ctx.strokeStyle = "#999";
	for(var i=0;i<segments.length;i++){
		var seg = segments[i];
		ctx.beginPath();
		ctx.moveTo(seg.a.x,seg.a.y);
		ctx.lineTo(seg.b.x,seg.b.y);
		ctx.stroke();
	}
	// Get all unique points
	var points = (function(segments){
		var a = [];
		segments.forEach(function(seg){
			a.push(seg.a,seg.b);
		});
		return a;
	})(segments);
	var uniquePoints = (function(points){
		var set = {};
		return points.filter(function(p){
			var key = p.x+","+p.y;
			if(key in set){
				return false;
			}else{
				set[key]=true;
				return true;
			}
		});
	})(points);

	for(var i=0;i<guards.length*2;i++){
		// Get all angles
		var uniqueAngles = [];
		for(var j=0;j<uniquePoints.length;j++){
				var uniquePoint = uniquePoints[j];
				var angle = Math.atan2(uniquePoint.y-visibilityGuard[i].y,uniquePoint.x-visibilityGuard[i].x);
				uniquePoint.angle = angle;
				uniqueAngles.push(angle-0.00001,angle,angle+0.00001);
		}

		// RAYS IN ALL DIRECTIONS
		var intersects = [];
		for(var j=0;j<uniqueAngles.length;j++){
				var angle = uniqueAngles[j];

				// Calculate dx & dy from angle
				var dx = Math.cos(angle);
				var dy = Math.sin(angle);

				// Ray from center of screen to mouse
				var ray = {
					a:{x:visibilityGuard[i].x,y:visibilityGuard[i].y},
					b:{x:visibilityGuard[i].x+dx,y:visibilityGuard[i].y+dy}
				};

				// Find CLOSEST intersection
				var closestIntersect = null;
				for(var k=0;k<segments.length;k++){
					var intersect = getIntersection(ray,segments[k]);
					if(!intersect) continue;
					if(!closestIntersect || intersect.param<closestIntersect.param){
						closestIntersect=intersect;
					}
				}

				// Intersect angle
				if(!closestIntersect) continue;
				closestIntersect.angle = angle;

				// Add to list of intersects
				intersects.push(closestIntersect);
		}

		// Sort intersects by angle
		intersects = intersects.sort(function(a,b){
			return a.angle-b.angle;
		});

		// DRAW AS A GIANT POLYGON
		ctx.fillStyle = "rgba(221,56,56,0.1)";
		ctx.beginPath();
		ctx.moveTo(intersects[0].x,intersects[0].y);
		for(var k=1;k<intersects.length;k++){
			var intersect = intersects[k];
			ctx.lineTo(intersect.x,intersect.y);
		}
		ctx.fill();
	}

	// DRAW DEBUG LINES
	ctx.strokeStyle = "black";
	for(var i=0;i<intersects.length;i++){
		for(var j=0;j<guards.length*2;j++){

			//var intersect = intersects[i];
			ctx.beginPath();
			ctx.arc(visibilityGuard[j].x,visibilityGuard[j].y,3,0, Math.PI*2);
			// ctx.lineTo(intersect.x,intersect.y); 
			ctx.stroke();
			ctx.fillStyle = "black";
			ctx.fill();

		}
	}
}
// DRAW LOOP
window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame;
var updateCanvas = true;
function drawLoop(){
    requestAnimationFrame(drawLoop);
    if(updateCanvas){
    	draw();
    	updateCanvas = false;
    }
}
window.onload = function(){
	scaleSegment();
	drawLoop();
	
};