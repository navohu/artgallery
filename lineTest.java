package artGallery;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class lineTest {

	public static Double [][] test = new Double [2][12];
	public static Double [][] guards = new Double [2][3];
	public static String [] blindspot;	

		
	public static String guardCheck(Double [][] vertices, Double [][] guards, int polynum){	
		HashMap<Integer, ArrayList<String>> blindSpot = new HashMap<Integer, ArrayList<String>>();	//create hashmap with arraylist for each guard

		for(int g = 0; g < guards[0].length; g++){
			blindSpot.put(g,intersectionCheck(guards[0][g],guards[1][g], vertices, polynum));		//creates arraylist for each guard. adds to hashmap
		}
		
		String spot = findRef(blindSpot, blindSpot.size());
		if(spot!=null){
			System.out.println(polynum+": We CANNOT see "+spot);
		}
		else{	
			spot = "(1.0, 1.0)";
			System.out.println(polynum+": We can see "+spot);
		}
		
		return spot;
	}
	
	public static String findRef(HashMap<Integer, ArrayList<String>> blindSpot, int len){
		String refutation="";

		
		for(int p = 0; p<blindSpot.get(0).size(); p++){	
			int match = 0;
			if(len==1){
				refutation = blindSpot.get(p).get(0);
				return refutation;
			}
			for(int k = 1; k<blindSpot.size(); k++){
				if(blindSpot.get(k).contains(blindSpot.get(0).get(p))==true){
					refutation = blindSpot.get(0).get(p);
				}
				else{
					match--;
					refutation = null;
					break;
				}					
			}
			if(match == 0){
				break;
			}
	    }	
		return refutation;
	}
	
	public static ArrayList<String> intersectionCheck(Double guardX, Double guardY, Double [][] vertices, int element){
		String answer = "";
		boolean result = false;
		Line2D guard;

		
		ArrayList<Line2D> lines = new ArrayList<Line2D>();
		ArrayList<String> blind = new ArrayList<String>();
		lines = polygonLines(vertices);	
		
		for(int guardLine = 0; guardLine < vertices[0].length; guardLine++){		//create a line from the guard to each vertex
			guard = new Line2D.Double(guardX,guardY,vertices[0][guardLine],vertices[1][guardLine]);
			Double x2 = guard.getX2();
			Double y2 = guard.getY2();
			
			for(int n = 0; n < lines.size(); n++){		//for each edge checks whether guard line intersects
				Line2D l = lines.get(n);
				result = guard.intersectsLine(l);
				if(element == 2){
					answer = "("+guard.getX2()+", "+guard.getY2()+")";
					blind.add(answer);
					return blind;
				}				
				else if((guardX==lines.get(n).getX1())&&(guardY==lines.get(n).getY1())){		//if guard is on point
					result = false;
					//result = reverseResult(result);
					//System.out.println("guard on point1");
				}
				else if((guardX==lines.get(n).getX2())&&(guardY==lines.get(n).getY2())){
					result = false;
					//result = reverseResult(result);
					//System.out.println("guard on point2");
				}
				else if((x2==lines.get(n).getX1())&&(y2==lines.get(n).getY1())){
					result = false;
					//result = reverseResult(result);
				}
				else if((x2==lines.get(n).getX2())&&(y2==lines.get(n).getY2())){
					result = false;
					//result = reverseResult(result);
				}
				else if(lineCheck(guardX, guardY,lines.get(n))==true){
					result = false;
					//result = reverseResult(result);
				}
				
				//System.out.println("edge "+(n+1)+" intersects: "+result);
				if(result==true){
					//System.out.println("I can't see everything");
					answer = "("+guard.getX2()+", "+guard.getY2()+")";
					blind.add(answer);
				}
				else{
					//System.out.println("I can see all");
				}
			}
		}
		
		return blind;	//return arrayList
	}
	
	public static Boolean reverseResult(boolean result){
		if(result==true){
			result = false;
		}
		else{
			result = true;
		}		
		return result;
	}
	
	
	public static ArrayList<Line2D> polygonLines(Double [][] coordinates){
		ArrayList<Line2D> lines = new ArrayList<Line2D>();
		int len = coordinates[0].length;
		Double x1,y1,x2,y2;
		for(int point = 0; point < len; point++){
			x1 = coordinates[0][point];
    		y1 = coordinates[1][point];
    		
			if(point == (len-1)){
	    		x2 = coordinates[0][0];
	    		y2 = coordinates[1][0];
			}
			else{
	    		x2 = coordinates[0][point+1];
	    		y2 = coordinates[1][point+1];
			}   
			
    		lines.add(point,new Line2D.Double(x1,y1,x2,y2));
    	}
		return lines;
	}
	
	public static Double equateGradient(Double x1, Double y1, Double x2, Double y2){
		Double gradient = (y2-y1)/(x2-x1);
		return gradient;
	}
	
public static Boolean lineCheck(Double pX, Double pY, Line2D line){	//checks if guard is on the line
		
		if(Line2D.ptLineDist(line.getX1(),line.getY1(),line.getX2(),line.getY2(),pX,pY)==0){
			//System.out.println("within the line");
			return true;
		}
		else{
			//System.out.println("nope");
			return false;
		}
	}
	
}




